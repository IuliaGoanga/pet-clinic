package main;

import dao.ConsultDao;
import dao.PetDao;
import dao.VetDao;
import entities.*;
import service.InputOutputService;
import service.UtilityService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class PetClinic {


    private VetDao vetDao = new VetDao();
    private PetDao petDao = new PetDao();
    private ConsultDao consultDao = new ConsultDao();
    private InputOutputService inputOutputService = new InputOutputService();
    private Scanner scanner = new Scanner(System.in);
    private UtilityService utilityService = new UtilityService();


    public void start() {
        while(true) {
            inputOutputService.displayMenu();
            String userInput = inputOutputService.getUserInput();
            if(isExitCondition(userInput)){
                break;
            }
            process(userInput);
        }
    }

    private boolean isExitCondition(String userInput) {
        return userInput.equalsIgnoreCase("x");
    }

    private void process(String userInput) {
        switch (userInput){
            case "1":{
                addVet();
                break;
            }
            case "2":{
                addPet();
                break;
            }
            case "3":{
                addConsult();
                break;
            }
            case "4":{
                addWorkInterval();
                break;
            }
        }
    }

    private void addWorkInterval() {
        System.out.println("Which vet would you like to add a work interval for?");
        List<Vet> allVets = vetDao.findAll();

        Integer vetIndex;
        inputOutputService.displayVets(allVets);
        while(true){
            System.out.print("Your answer: ");
            String veterinaryIndexAsString = scanner.nextLine();
            Optional<Integer> optionalVeterinaryIndex = parseVeterinaryIndex(veterinaryIndexAsString);

            if(optionalVeterinaryIndex.isPresent()){ // cand intram in "if", cutia sigur are ceva in ea. altfel era exceptie pe "empty".
                vetIndex = optionalVeterinaryIndex.get();
                if(isIndexValid(allVets, vetIndex)){
                    break;
                }
            }
            System.out.println("Invalid index. Please try again.");
        }
        Vet vet = allVets.get(vetIndex-1);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("H:mm");
        DayOfWeek[] days = DayOfWeek.values();
        for(DayOfWeek day : days){
 //           if(day == DayOfWeek.SUNDAY){
 //               continue;
 //           }
            System.out.print("Please insert start hour for " + day.name() + ": ");
            String startHourAsString = scanner.nextLine();
            System.out.print("Please insert end hour for " + day.name() + ": ");
            String endHourAsString = scanner.nextLine();

            LocalTime startHour = LocalTime.parse(startHourAsString, dateTimeFormatter);
            LocalTime endHour = LocalTime.parse(endHourAsString, dateTimeFormatter);

            Optional<WorkDaySchedule> optionalWorkDaySchedule = vet.getScheduleFor(day); //da-mi programul pt ziua de luni
            if(optionalWorkDaySchedule.isPresent()){ // daca cutia are continut
                WorkDaySchedule workDaySchedule = optionalWorkDaySchedule.get(); // despachetez
                workDaySchedule.setStartTime(startHour); // si schimb ora
                workDaySchedule.setEndTime(endHour);
            }else{ // cazul in care vet nu are program setat inca
                WorkDaySchedule workDaySchedule = new WorkDaySchedule(startHour,endHour,day,vet);
                vet.addSchedule(workDaySchedule);
            }
        }
        vetDao.merge(vet);
    }

    private void addConsult() {
        Vet vet = getVet();

        Optional<WorkDaySchedule> optionalWorkDaySchedule;
        LocalDate consultDate;
        while(true) {
            Optional<LocalDate> optionalDate = getConsultDate();
            if (optionalDate.isPresent()) {
                consultDate = optionalDate.get();
                optionalWorkDaySchedule = getWorkDaySchedule(vet, consultDate);
                if (optionalWorkDaySchedule.isPresent()) {
                    break;
                }
            }
        }
        WorkDaySchedule workDaySchedule = optionalWorkDaySchedule.get();
        List<LocalTime> scheduledConsultationHours = getScheduledConsultationHours(vet, consultDate);

        List<LocalTime> availableForScheduling = getAvailableSchedulingHours(workDaySchedule, scheduledConsultationHours);

        //acum stim concret daca avem sau nu optiuni de programare
        if(availableForScheduling.isEmpty()) {
            System.out.println("All booked for this day. Please try another day.");
            return;
        }
        LocalTime consultTime = getConsultTime(availableForScheduling);
        String ownerName = inputOutputService.read("your name");
        String petName = inputOutputService.read("your pet's name");
        Optional<Pet> optionalPet = petDao.findByOwnerNameAndPetName(ownerName,petName);
        if(!optionalPet.isPresent()){
            System.out.println("Sorry, there is no pet in our DB with those details");
            return; // sanatate si un praz verde! iesim din metoda.
        }
        Pet pet = optionalPet.get(); // despachetam
        String description = inputOutputService.read("the reason of the consult");
        LocalDateTime dateTime = LocalDateTime.of(consultDate,consultTime);

        Consult consult = new Consult(vet,pet,dateTime,description);
        consultDao.save(consult);
        System.out.println("Your consult has been scheduled!");
    }

    private LocalTime getConsultTime(List<LocalTime> availableForScheduling) {
        LocalTime consultTime;
        while(true) {
            inputOutputService.displayAvailableTimeForConsult(availableForScheduling);
            String consultTimeAsString = inputOutputService.readYourAnswer();
            consultTime = utilityService.parseTime(consultTimeAsString);
            if(availableForScheduling.contains(consultTime)){
                return consultTime;
            }
            System.out.println("Start time outside availability.");
        }
    }

    private List<LocalTime> getAvailableSchedulingHours(WorkDaySchedule workDaySchedule, List<LocalTime> scheduledConsultationHours) {
        List<LocalTime> availableForScheduling = new ArrayList<>(); //lista e goala
        LocalTime candidateTime = workDaySchedule.getStartTime();
        while(candidateTime.isBefore(workDaySchedule.getEndTime())){ //cat inca n am ajuns la end time
            if(!scheduledConsultationHours.contains(candidateTime)){ // daca candidate time nu e deja booked, putem inca programa o consultatie
                availableForScheduling.add(candidateTime);
            }
            candidateTime = candidateTime.plusHours(1);
        }
        return availableForScheduling;
    }

    private List<LocalTime> getScheduledConsultationHours(Vet vet, LocalDate consultDate) {
        //folosim stream-uri
        return vet.getConsultList().stream()
                .filter(consult -> consult.getDateTime().toLocalDate().equals(consultDate))
                .map(Consult::getTime) // ma intereseaza doar timpul programarilor
                .collect(Collectors.toList());
    }

    private Optional<WorkDaySchedule> getWorkDaySchedule(Vet vet, LocalDate consultDate) {
        Optional<WorkDaySchedule> optionalWorkDaySchedule = vet.getScheduleFor(consultDate.getDayOfWeek());
        if(!optionalWorkDaySchedule.isPresent()){
            System.out.println("The vet is not available on this date.");
        }
        return optionalWorkDaySchedule;
    }

    private Optional<LocalDate> getConsultDate() {
        String dateForConsultAsString = inputOutputService.readDate("date for consult");
        return utilityService.parseDate(dateForConsultAsString);
    }

    private Vet getVet() {
        List<Vet> allVets = vetDao.findAll();

        inputOutputService.displayVetsAppointmentMessage(allVets);
        while(true){ //bucla infinita
            String veterinaryIndexAsString = inputOutputService.readYourAnswer();
            Optional<Integer> optionalVeterinaryIndex = parseVeterinaryIndex(veterinaryIndexAsString);

            if(optionalVeterinaryIndex.isPresent()){ // cand intram in "if", cutia sigur are ceva in ea. altfel era exceptie pe "empty".
                int vetIndex = optionalVeterinaryIndex.get();
                if(isIndexValid(allVets, vetIndex)){
                    return allVets.get(vetIndex-1);
                }
            }
           inputOutputService.displayErrorMessage("Index");
        }
    }

    private boolean isIndexValid(List<Vet> allVets, int vetIndex) {
        return vetIndex > 0 && vetIndex <= allVets.size();
    }

    private Optional<Integer> parseVeterinaryIndex(String veterinaryIndexAsString) {
        try{
            int veterinaryIndex = Integer.parseInt(veterinaryIndexAsString);
            return Optional.of(veterinaryIndex);
        } catch(NumberFormatException e){
            return Optional.empty();
        }
    }

    private void addPet() {
        String petFirstName = inputOutputService.readPet("first name");
        LocalDate petBirthDate = getPetBirthDate();
        PetType petType = getPetType();
        boolean petIsVaccinated = getIsVaccinated();
        String ownerName = inputOutputService.readPet("owner name");

        Pet pet = new Pet(petFirstName, petBirthDate, petType, petIsVaccinated, ownerName);
        petDao.save(pet);
    }

    private boolean getIsVaccinated() {
        String petIsVaccinatedAsString = inputOutputService.readPet("vaccinated: 'yes/no'");
        return parseIsVaccinated(petIsVaccinatedAsString);
    }

    private PetType getPetType() {
        while(true) {
            String petTypeAsString = inputOutputService.readPet("type");
            Optional<PetType> optionalPetType = parsePetType(petTypeAsString); // tb ca utilizatorul sa aleaga un enum. nu poate alege "crocodil",
            // deoarece pet clinic nu se ocupa de crocodili.
            if(optionalPetType.isPresent()){
                return optionalPetType.get();
            }
            inputOutputService.displayErrorMessage("Pet type");
        }
    }

    private LocalDate getPetBirthDate() {
        while(true){
            String petBirthDateAsString = inputOutputService.readDate("pet birth date");
            Optional<LocalDate> optionalPetBirthDate = utilityService.parseDate(petBirthDateAsString); // variabila -primeste- metoda parseBirthDate.
            if(optionalPetBirthDate.isPresent()){
                return optionalPetBirthDate.get();
            }
            inputOutputService.displayErrorMessage("Date");
        }
    }

    private boolean parseIsVaccinated(String petIsVaccinatedAsString) {
        if(petIsVaccinatedAsString.equalsIgnoreCase("Y")){
            return true;
        } else {
            return false;
        }
    }

    private Optional<PetType> parsePetType(String petTypeAsString) {
        try{
            // Integer x = 1 + 2; // -> spunem: x -primeste- 3 -sau- 3 va fi -asignat / atribuit- variabilei x.
            // == -> egalitate, -e egal cu-.
            PetType petType = PetType.valueOf(petTypeAsString.toUpperCase());
            return Optional.of(petType);
        } catch(IllegalArgumentException e){
            return Optional.empty();
        }
    }

    private void addVet() {
        String vetFirstName = inputOutputService.readVet("first name");
        String vetLastName = inputOutputService.readVet("last name");
        String vetAddress = inputOutputService.readVet("address");
        String vetSpecialities = inputOutputService.readVet("specialities separated by space");
        Vet vet = new Vet(vetFirstName, vetLastName, vetAddress, vetSpecialities);
        vetDao.save(vet);
    }
}
