package main;

import util.HibernateUtil;

public class Main {
    public static void main(String[] args) {
        PetClinic petClinic = new PetClinic();
        petClinic.start();
    }
}
