package dao;

import entities.Pet;
import org.hibernate.Session;

import org.hibernate.query.Query;
import java.util.Optional;

public class PetDao extends GenericDao<Pet>{

    public Optional<Pet> findByOwnerNameAndPetName(String ownerName, String petName) {
        Session session = sessionFactory.openSession();
        Query<Pet> query = session.createQuery("select p from Pet p where p.name = :petName and p.ownerName = : ownerName");
        // : -> urmeaza un parametru
        query.setParameter("petName", petName);
        query.setParameter("ownerName", ownerName);
        return query.uniqueResultOptional(); // o sa arunce exceptie daca avem 2 pets cu acelasi nume si acelasi owner name
        // query se asteapta la un unique result
    }
}
