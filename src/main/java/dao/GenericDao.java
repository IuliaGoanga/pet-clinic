package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class GenericDao<X> {
    protected SessionFactory sessionFactory = HibernateUtil.getSessionFactory(); // am creat clasa HibernateUtil cu metoda getSessionF!
    // .getSessionFactory e statica, asa ca o putem apela cu ajutorul clasei!!!!!!!!!

    public void save(X someObject) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(someObject);
        transaction.commit();
        session.close();
    }
    //save = insert
    //update = update
    //merge = save pt vet + insert pt ...

    public void merge(X someObject) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(someObject);
        transaction.commit();
        session.close();
    }
}
