package dao;

import entities.Vet;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class VetDao extends GenericDao<Vet>{

    public List<Vet> findAll() {
        Session session = sessionFactory.openSession();
        Query<Vet> vetQuery = session.createQuery("from Vet");
        return vetQuery.list();
    }
}
