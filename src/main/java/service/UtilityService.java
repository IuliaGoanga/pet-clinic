package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

public class UtilityService {
    public Optional<LocalDate> parseDate(String dateAsString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy");
        try{
            LocalDate result = LocalDate.parse(dateAsString, formatter);
            return Optional.of(result);
        }
        catch (DateTimeParseException e){
            return Optional.empty();
        }
    }
    public LocalTime parseTime(String consultTimeAsString) {
        LocalTime consultTime;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("H:mm");
        consultTime = LocalTime.parse(consultTimeAsString, dateTimeFormatter);
        return consultTime;
    }
}
