package service;

import entities.Vet;

import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

public class InputOutputService {
    private Scanner scanner = new Scanner(System.in);

    public void displayMenu() {
        System.out.println("Welcome to our pet clinic. Please choose one of the following options:");
        System.out.println("1 - Add vet");
        System.out.println("2 - Add pet");
        System.out.println("3 - Schedule a consult");
        System.out.println("4 - Add work interval for vet");
        System.out.println("x - Exit");
    }
    public String getUserInput() {
        System.out.print("Your answer: ");
        return scanner.nextLine();
    }
    public String readVet(String detail) {
        return read("vet", detail);
    }

    public String readPet(String detail) {
        return read("pet", detail);
    }

    public String read(String element, String detail) {
        System.out.print("Please enter a " + element + " " + detail + ": ");
        return scanner.nextLine();
    }

    public String read(String detail) {
        System.out.print("Please enter " + detail + ": ");
        return scanner.nextLine();
    }


    public String readDate(String detail){
        System.out.print("Please enter a " + detail + " in DD-MM-YYYY format: ");
        return scanner.nextLine();
    }

    public void displayErrorMessage(String element){
        System.out.println(element + " not valid, please try again.");
    }

    public void displayVetsAppointmentMessage(List<Vet> allVets) {
        System.out.println("Which vet would you like to create an appointment with?");
        displayVets(allVets);
    }

    public void displayVets(List<Vet> allVets) {
        for (int index = 0; index < allVets.size(); index++) {
            System.out.println((index + 1) + " - " + allVets.get(index).getLastName());
        }
    }

    public String readYourAnswer() {
        System.out.print("Your answer: ");
        return scanner.nextLine();
    }

    public void displayAvailableTimeForConsult(List<LocalTime> availableForScheduling) {
        System.out.print("Please pick one of the following start time for the consult: ");
        for (LocalTime localTime : availableForScheduling) {
            System.out.print(localTime + " ");
        }
        System.out.println();
    }

}
