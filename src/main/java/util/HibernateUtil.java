package util;

import entities.Consult;
import entities.Pet;
import entities.Vet;
import entities.WorkDaySchedule;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory = null; // Singleton. daca vreau sa am o clasa, un tipar pe baza caruia sa
    // pot construi un singur obiect. clasa "Sun". nu pot instantia de mai multe ori Sun, tb sa am un singur soare.
    // Singleton - mecanism prin care pot garanta ca am un singur obiect format pe baza unei clase.
    // in cazul de fata avem un singur obiect sessionFactory. Totul este static. nu vrem sa cream mai multe conexiuni la baza de date. Doar una.
    public static SessionFactory getSessionFactory(){
        if(sessionFactory == null){ // prima data cand pornim aplicatia, sessionFactory va fi null. asa ca se apeleaza if-ul
            // condul de instantiere -> sessionFactory va fi creat o singura data (Singleton)
            initSessionFactory();
        }
        return sessionFactory;
    }
    private static void initSessionFactory() {
        /*START BOILER PLATE CODE */
        Configuration configuration = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/pet_clinic?useSSL=false");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "MYSQLPASS");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL55Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");

        configuration.addAnnotatedClass(Consult.class);
        configuration.addAnnotatedClass(Pet.class);
        configuration.addAnnotatedClass(Vet.class);
        configuration.addAnnotatedClass(WorkDaySchedule.class);

        configuration.setProperties(settings);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        /*END BOILER PLATE CODE*/
    }
}
