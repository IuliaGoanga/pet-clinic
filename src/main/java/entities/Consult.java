package entities;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
public class Consult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn // spunem hibernate-ului sa creeze o coloana in care sa stocheze info despre vet
    private Vet vet;

    @ManyToOne
    @JoinColumn
    private Pet pet;

    private LocalDateTime dateTime;
    private String description;

    public Consult() {}

    public Consult(Vet vet, Pet pet, LocalDateTime dateTime, String description) {
        this.vet = vet;
        this.pet = pet;
        this.dateTime = dateTime;
        this.description = description;
    }

    public Vet getVet() {
        return vet;
    }

    public void setVet(Vet vet) {
        this.vet = vet;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public LocalTime getTime() {
        return dateTime.toLocalTime();
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
