package entities;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
public class Vet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private String address;

    @ElementCollection(targetClass = PetType.class) // vreau sa pun mai multe elemente de tipul PetType.class
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "vet_speciality")
    private List<PetType> specialityList;

    @OneToMany(mappedBy = "vet", cascade = CascadeType.ALL)
    private List<Consult> consultList;

    @OneToMany(mappedBy = "vet", cascade = CascadeType.ALL)
    private List<WorkDaySchedule> workDayScheduleList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PetType> getSpecialityList() {
        return specialityList;
    }

    public void setSpecialityList(List<PetType> specialityList) {
        this.specialityList = specialityList;
    }

    public List<Consult> getConsultList() {
        return consultList;
    }

    public void setConsultList(List<Consult> consultList) {
        this.consultList = consultList;
    }

    public List<WorkDaySchedule> getWorkDayScheduleList() {
        return workDayScheduleList;
    }

    public void setWorkDayScheduleList(List<WorkDaySchedule> workDayScheduleList) {
        this.workDayScheduleList = workDayScheduleList;
    }

    public Vet(){}

    public Vet(String firstName, String lastName, String address, String specialities) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.specialityList = convertToEnumList(specialities);

    }

    private List<PetType> convertToEnumList(String specialities) {
        String[] specialitiesArray = specialities.split(" ");
        List<PetType> result = new ArrayList<>();
        for(String speciality : specialitiesArray){
            try {
                PetType petType = PetType.valueOf(speciality.toUpperCase());
                result.add(petType);
            } catch(IllegalArgumentException e){
                System.out.println(speciality + " is not a valid speciality");
            }
        }
        return result;
    }

    public Optional<WorkDaySchedule> getScheduleFor(DayOfWeek day) {
        for(WorkDaySchedule workDaySchedule : workDayScheduleList){
            if(workDaySchedule.getDayOfWeek().equals(day)){
                return Optional.of(workDaySchedule);
            }
        }
        return Optional.empty();
    }

    public void addSchedule(WorkDaySchedule workDaySchedule) {
        workDayScheduleList.add(workDaySchedule);
    }
}
