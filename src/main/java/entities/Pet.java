package entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private PetType type;

    private boolean isVaccinated;
    private String ownerName;

    @OneToMany(mappedBy = "pet")
    private List<Consult> consultList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public PetType getType() {
        return type;
    }

    public void setType(PetType type) {
        this.type = type;
    }

    public boolean isVaccinated() {
        return isVaccinated;
    }

    public void setVaccinated(boolean vaccinated) {
        isVaccinated = vaccinated;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pet(){} // constructor default, gol

    public Pet(String name, LocalDate birthDate, PetType type, boolean isVaccinated, String ownerName) {
        this.name = name;
        this.birthDate = birthDate;
        this.type = type;
        this.isVaccinated = isVaccinated;
        this.ownerName = ownerName;
    }


}
