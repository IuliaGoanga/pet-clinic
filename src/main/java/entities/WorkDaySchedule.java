package entities;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;


@Entity
public class WorkDaySchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private LocalTime startTime;
    private LocalTime endTime;
    private DayOfWeek dayOfWeek;

    @ManyToOne
    @JoinColumn
    private Vet vet;

    public WorkDaySchedule(){}

    public WorkDaySchedule(LocalTime startTime, LocalTime endTime, DayOfWeek dayOfWeek, Vet vet) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.dayOfWeek = dayOfWeek;
        this.vet = vet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Vet getVet() {
        return vet;
    }

    public void setVet(Vet vet) {
        this.vet = vet;
    }
}
