package entities;

public enum PetType {
    DOG,
    CAT,
    HORSE,
    PARROT,
    HAMSTER;
}
